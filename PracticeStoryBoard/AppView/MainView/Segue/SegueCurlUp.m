//
//  SegueCurlUp.m
//  PracticeStoryBoard
//
//  Created by ShogoMizumoto on 2014/08/09.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import "SegueCurlUp.h"

@implementation SegueCurlUp

- (void)perform
{
    UIViewController *sourceViewController      = (UIViewController *)self.sourceViewController;
    UIViewController *destinationViewController = (UIViewController *)self.destinationViewController;

    [UIView transitionWithView:sourceViewController.navigationController.view
                      duration:0.3f
                       options:UIViewAnimationOptionTransitionCurlUp
                    animations:^{
                        [sourceViewController.navigationController pushViewController:destinationViewController animated:NO];
                    }
                    completion:nil];
}

@end
