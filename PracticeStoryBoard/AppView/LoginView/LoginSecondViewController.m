//
//  LoginSecondViewController.m
//  PracticeStoryBoard
//
//  Created by ShogoMizumoto on 2014/08/09.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import "LoginSecondViewController.h"

@implementation LoginSecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.uLabelLoginName.text = [[NSString alloc] initWithFormat:@"LoginName:%@", self.mLoginName];
}

- (IBAction)tapStartLogin
{
    NSLog(@"call tapStartLogin.");

    UIStoryboard     *storyboard = [UIStoryboard storyboardWithName:@"MainStory" bundle:nil];
    UIViewController *controller = [storyboard instantiateInitialViewController];


    [self presentViewController:controller animated:YES completion:NULL];
}

- (IBAction)tapNavigationBack
{
    NSLog(@"call tapNavigationBack.");
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    NSLog(@"call shouldPerformSegueWithIdentifier. Identifier:%@", identifier);
    return YES;
}

@end
