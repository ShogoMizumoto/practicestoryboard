//
//  SegueFlipLeft.m
//  PracticeStoryBoard
//
//  Created by ShogoMizumoto on 2014/08/09.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import "SegueFlipLeft.h"

@implementation SegueFlipLeft

- (void)perform
{
    UIViewController *sourceViewController      = (UIViewController *)self.sourceViewController;
    UIViewController *destinationViewController = (UIViewController *)self.destinationViewController;

    [UIView transitionWithView:sourceViewController.navigationController.view
                      duration:0.3f
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        [sourceViewController.navigationController pushViewController:destinationViewController animated:NO];
                    }
                    completion:nil];
}

@end
