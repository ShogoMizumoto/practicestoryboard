//
//  LoginSecondViewController.h
//  PracticeStoryBoard
//
//  Created by ShogoMizumoto on 2014/08/09.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//



@interface LoginSecondViewController : UIViewController

@property NSString *mLoginName;
@property (nonatomic, strong) IBOutlet UILabel *uLabelLoginName;

- (IBAction)tapStartLogin;
- (IBAction)tapNavigationBack;

@end
