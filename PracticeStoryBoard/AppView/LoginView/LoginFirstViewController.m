//
//  LoginFirstViewController.m
//  PracticeStoryBoard
//
//  Created by ShogoMizumoto on 2014/08/09.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import "LoginFirstViewController.h"
#import "LoginSecondViewController.h"

static NSString *const kSegue_LoginSecondView = @"SequeLoginSecondView";

@implementation LoginFirstViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    NSLog(@"call shouldPerformSegueWithIdentifier. Identifier:%@", identifier);

    if ([identifier isEqualToString:kSegue_LoginSecondView]) {
        NSLog(@"TextField Text:%@", self.uFieldName.text);
        [self hideKeyboard];
        if (self.uFieldName.text.length != 0) {
            [self performSegueWithIdentifier:kSegue_LoginSecondView sender:self];
        }
    }

    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"call prepareForSegue. Seque:%@ Sender:%@", segue, sender);

    LoginSecondViewController *vc = [segue destinationViewController];
    vc.mLoginName = self.uFieldName.text;
    [self hideKeyboard];
}

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

@end
